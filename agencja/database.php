<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016-10-21
 * Time: 15:32
 */

DEFINE("__HOST__","localhost");
DEFINE("__USERNAME__","root");
DEFINE("__PASSWORD__","");
DEFINE("__DB_NAME__","agencja");

function connect_db($host,$uname,$pass,$dbname){
    $connect = mysqli_connect($host,$uname,$pass);

    if(!$connect){
        die("Błąd połączenia".mysqli_connect_error()." ERNO: ".mysqli_connect_errno());
    }else {
        $db_select = mysqli_select_db($connect, $dbname);

        if (!$db_select) {
            die("Błąd połączenia".mysqli_connect_error()." ERNO: ".mysqli_connect_errno());
        } else {
            echo "Połączenie z bazą danych nawiązane ",$host, "<br>";
            echo "Wybrano bazę danych: ", $dbname;

            return $connect;
        }
    }
}

$connect = connect_db(__HOST__, __USERNAME__, __PASSWORD__, __DB_NAME__);