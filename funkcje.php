<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016-10-21
 * Time: 15:46
 */

function walidator($klucz) {
    if (!isset($_POST[$klucz]) || empty($_POST[$klucz])) {
        return 'Pole nie może być puste';
    } else if (strlen($_POST[$klucz]) <= 3) {
        return 'Pole za krótkie';
    }

    return false;
}

function sprawdz($errors) {
    if (!empty($errors)) {
        foreach ($errors as $error) {
            if ($error) {
                return false;
            }
        }
    }

    return true;
}

function filtr($wartosc) {
    return str_replace(array('kot', 'pies', 'dzik'), '***', $wartosc);
}

function input($name, $post, $errors = false) {
    $html = '<input type="text" name="' . $name . '" value="' . (isset($post[$name]) ? $post[$name] : '') . '" />';


    if ($errors) {
        if (isset($errors[$name]) && $errors[$name] !== false) {
            $html .= '<small style="color: red">' . $errors[$name] . '</small>';
        }
    }

    return $html;
}