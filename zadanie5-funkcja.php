<pre>
    <?php
    //include 'baza.php';
    include 'funkcje.php';

    if (isset($_POST) && !empty($_POST)) {
        $errors = array();
        $post = array();


        foreach ($_POST as $klucz => $vartosc) {
            $errors[$klucz] = walidator($klucz);
        }

        // sprawdzenie poprawności maila
        if (!$errors['email']) {
            if (strpos($_POST['email'], '@') == false || strpos($_POST['email'], '.') == false) {
                $errors['email'] = "Nieprawidłowy adres email";
            }
        }

        // sprawdzenie poprawności kodu pocztowego
        if (!$errors['kod']) {
            $kod = $_POST['kod'];
            $posMyslinika = strpos($kod, '-');

            if ($posMyslinika == false) {
                $errors['kod'] = "Nieprawidłowy kod pocztowy";
            } else {
                $znaki1 = (int) substr($kod, 0, 2);
                $znaki2 = (int) substr($kod, -3);

                if (strlen($znaki1) != 2 || strlen($znaki2) != 3 || strlen($kod) != 6) {
                    $errors['kod'] = "Nieprawidłowy kod pocztowy";
                }
            }
        }

        if (sprawdz($errors)) {
            $daneDoZapisu = array();
            // Pierwsza litera duża oraz brak pustych znaków " "
            $daneDoZapisu['imie'] = trim(ucfirst($_POST['imie']));
            // Pierwsza litera duża oraz brak pustych znaków " "
            $daneDoZapisu['nazwisko'] = trim(ucfirst($_POST['nazwisko']));
            // Wszystkie pierwsze litera słów z dużej oraz brak pustych znaków " "
            $daneDoZapisu['miasto'] = trim(ucwords($_POST['miasto']));
            // wszystkie z małej oraz brak pustych znaków " "
            $daneDoZapisu['email'] = trim(strtolower($_POST['email']));
            // tylko cyfry oraz brak pustych znaków " "
            $daneDoZapisu['tel'] = trim(intval($_POST['tel']));

            print_r($daneDoZapisu);
        } else {
            echo "<h1>Popraw formularz</h1>";
        }
    }
    ?>
</pre>

<!DOCTYPE html>
<html>
<head>
    <title>Formularz</title>
    <meta charset="UTF-8">
</head>
<body>
<form method="post">
    <table>
        <?php
        $result = mysqli_query("SELECT * FROM products ORDER BY price DESC", $link);

        if ($result) {
            while ($row = mysqli_fetch_assoc($result)) {
                ?>
                <tr>
                    <td>Nazwa produktu: </td>
                    <td>
                        <?php echo input('name', $row); ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        <tr>
            <td>Nazwa produktu: </td>
            <td>
                <?php echo input('imie', $_POST, $errors); ?>
            </td>
        </tr>
        <tr>
            <td>Imię: </td>
            <td>
                <?php echo input('imie', $_POST, $errors); ?>
            </td>
        </tr>
        <tr>
            <td>Nazwisko: </td>
            <td>
                <?php echo input('nazwisko', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td>Miasto: </td>
            <td>
                <?php echo input('miasto', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td>E-mail: </td>
            <td>
                <?php echo input('email', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td>Kod pocztowy: </td>
            <td>
                <?php echo input('kod', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td>Numer telefonu: </td>
            <td>
                <?php echo input('tel', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td></td>
            <td><input type="submit" /></td>
        </tr>
    </table>
</form>
</body>
</html>
<?php mysqli_close($link); ?>