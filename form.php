<pre>
    <?php

    function walidator($klucz) {
        if (!isset($_POST[$klucz]) || empty($_POST[$klucz])) {
            return 'Pole nie może być puste';
        } else if (strlen($_POST[$klucz]) <= 3) {
            return 'Pole za krótkie';
        }

        return false;
    }

    function sprawdz($errors) {
        if (!empty($errors)) {
            foreach ($errors as $error) {
                if ($error) {
                    return false;
                }
            }
        }

        return true;
    }

    function filtr($wartosc) {
        return str_replace(array('kot', 'pies', 'dzik'), '***', $wartosc);
    }

    function input($name, $post, $errors) {
        $html = '<input type="text" name="' . $name . '" value="' . (isset($post[$name]) ? $post[$name] : '') . '" />';


        if (isset($errors[$name]) && $errors[$name] !== false) {
            $html .= '<small style="color: red">' . $errors[$name] . '</small>';
        }

        return $html;
    }

    if (isset($_POST) && !empty($_POST)) {
        $errors = array();
        $post = array();

        foreach($_POST as $klucz => $wartosc){
            $errors[$klucz] = walidator($klucz);
        }

        if (sprawdz($errors)) {



            // fitrowanie
            // Dla pola Imię, pierwsza litera musi być duża, a następne po niej małe
            // Dla pola nazwisko -||-
            // Miasto: zielona góra => Zielona Góra
            // Email - wszystkie znaki małe oraz czy istnieje @ i .
            // numer telegonu => tylko cyfry


            $daneDoZapisu = array();
            $daneDoZapisu['imie'] = ucfirst($_POST['imie']);
            $daneDoZapisu['nazwisko'] = ucfirst($_POST['nazwisko']);
            $daneDoZapisu['email'] = 



        } else {
            echo "<h1>Popraw formularz</h1>";
        }
    }
    ?>
</pre>

<!DOCTYPE html>
<html>
<head>
    <title>Formularz</title>
    <meta charset="UTF-8">
</head>
<body>
<form method="post">
    <table>
        <tr>
            <td>Imię: </td>
            <td>
                <?php echo input('imie', $_POST, $errors); ?>
            </td>
        </tr>
        <tr>
            <td>Nazwisko: </td>
            <td>
                <?php echo input('nazwisko', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td>Miasto: </td>
            <td>
                <?php echo input('miasto', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td>E-mail: </td>
            <td>
                <?php echo input('email', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td>Kod pocztowy: </td>
            <td>
                <?php echo input('kod', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td>Numer telefonu: </td>
            <td>
                <?php echo input('tel', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td></td>
            <td><input type="submit" /></td>
        </tr>
    </table>
</form>
</body>
</html>