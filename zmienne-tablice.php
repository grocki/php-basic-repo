<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016-10-19
 * Time: 08:46
 */
echo "<h1>Tablice uporządkowane</h1>";
$warzywa = array(
    "Burak",
    "Kartofel",
    "Brokuł",
    "Marchewka");

echo "<p>Wybrane warzywo z tablicy: ", $warzywa[0], $warzywa[2], "</p>";

echo "<h1>Tablice asocjacyjne</h1>";
$osoba = array();
$osoba["imie"] = "Przemysław";
$osoba["nazwisko"] = "Włodkowski";
$osoba["telefon"] = "+48_604_750_869";
$osoba["email"] = "przemyslaw@wlodkowski.pl";
echo "<p>";
echo "Imię: ", $osoba["imie"], "<br>";
echo "Nazwisko: ", $osoba["nazwisko"], "<br>";
echo "Telefon: ", $osoba["telefon"], "<br>";
echo "E-mail: ", $osoba["email"], "<br>";
echo "</p>";
echo "<h1>Tablice wielowymiarowe</h1>";
$osoby = array();
//przemysław
$osoby[1]["imie"] = "Przemysław";
$osoby[1]["nazwisko"] = "Włodkowski";
$osoby[1]["telefon"] = "+48_604_750_869";
$osoby[1]["email"] = "przemyslaw@wlodkowski.pl";
//tomasz
$osoby[2]["imie"] = "Tomasz";
$osoby[2]["nazwisko"] = "Zieliński";
$osoby[2]["telefon"] = "+48_666_860_567";
$osoby[2]["email"] = "przemyslaw@wlodkowski.pl";
//grzegorz

//wybieranie
echo "<p>";
echo "Imię: ", $osoby[1]["imie"], "<br>";
echo "Nazwisko: ", $osoby[1]["nazwisko"], "<br>";
echo "Telefon: ", $osoby[1]["telefon"], "<br>";
echo "E-mail: ", $osoby[1]["email"], "<br>";
echo "</p>";
echo "<p>";
echo "Imię: ", $osoby[2]["imie"], "<br>";
echo "Nazwisko: ", $osoby[2]["nazwisko"], "<br>";
echo "Telefon: ", $osoby[2]["telefon"], "<br>";
echo "E-mail: ", $osoby[2]["email"], "<br>";
echo "</p>";

//debug
echo "<h1>debug</h1>";
echo "<pre>";
print_r($osoby);
echo "</pre>";

$osoby[] = array(
    "imie"=>"Tomasz",
    "nazwisko"=>"Włodkowski",
    "telefon"=>"666_888_999",
    "email"=>"tomasz@wlodkowski.pl"

);
$osoby[] = array(
    "imie"=>"Grzegorz",
    "nazwisko"=>"Rocki",
    "telefon"=>"608_013_309",
    "email"=>"rockigrzegorz@gmail.com"
);

$osoby[] = array(
    "imie"=>"Danuta",
    "nazwisko"=>"Jakubowska",
    "telefon"=>"602_120_677",
    "email"=>"djakubowska@o2.pl"
);
echo "<h1>debug</h1>";
echo "<pre>";
print_r($osoby);
echo "</pre>";

echo "<h1>Sortowanie</h1>";
$warzywa = array();
$warzywa = array(
    "Burak",
    "Kartofel",
    "Brokuł",
    "Marchewka");
//funkcja sort
sort($warzywa);

echo "<pre>";
print_r($warzywa);
echo "</pre>";

$liczby = array();
$liczby = array(1,4,7,9,6,3,5,2,8);
sort($liczby);

echo "<pre>";
print_r($liczby);
echo "</pre>";
//funkcja rsort

rsort($liczby);

echo "<pre>";
print_r($liczby);
echo "</pre>";

//funkcja asort - sortowanie po wartościach
echo "<h1>Sortowanie po wartościach</h1>";
asort($osoba);
echo "<pre>";
print_r($osoba);
echo "</pre>";

//funkcja ksort - sortowanie po kluczach
echo "<h1>Sortowanie po kluczach</h1>";
ksort($osoba);
echo "<pre>";
print_r($osoba);
echo "</pre>";

//$dziennik

//Dziennik Zajęć Placówki Kształcenia Ustawicznego GOWORK.PL
echo "<h1>Funkcja dzielenie wyrazów php - explode - i sortowanie</h1>";
echo "<h3>Dziennik Zajęć Placówki Kształcenia Ustawicznego GOWORK.PL</h3>";
$dane = "Dziennik Zajęć Placówki Kształcenia Ustawicznego GOWORK.PL";
$dane_wynikowe = explode(" ", $dane);
echo $dane_wynikowe[0]."<br/>";
echo $dane_wynikowe[1]."<br/>";
echo $dane_wynikowe[2]."<br/>";
sort ($dane_wynikowe);
echo "<pre>";
print_r($dane_wynikowe);
echo "</pre>";

echo "<h1>Funkcja implode</h1>";
$dane_polaczone = implode(" ",$dane_wynikowe);
echo "<pre>";
print_r($dane_polaczone);
echo "</pre>";

//dodawanie wartości do tablicy
echo "<h1>Array_Push - dodawanie wartości do tabeli</h1>";
array_push($dane_wynikowe, "www", "email");
echo "<pre>";
print_r($dane_wynikowe);
echo "</pre>";

//usuwanie wartości z tablicy
echo "<h1>Unset - usuwanie wartości z tablicy</h1>";

unset($dane_wynikowe[0]);
echo "<pre>";
print_r($dane_wynikowe);
echo "</pre>";

//usuwanie ostatniej pozycji z tablicy - array_pop
echo "<h1>usuwanie ostatniej pozycji z tablicy - array_pop</h1>";
array_pop($dane_wynikowe);
echo "<pre>";
print_r($dane_wynikowe);
echo "</pre>";











