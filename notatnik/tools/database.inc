
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016-10-21
 * Time: 11:43
 */
//require "./config.inc";


function connect_db($host,$uname,$pass,$dbname){
    $connect = mysqli_connect($host,$uname,$pass);

    if(!$connect){
        die("Błąd połączenia".mysqli_connect_error()." ERNO: ".mysqli_connect_errno());
    }else {
        $db_select = mysqli_select_db($connect, $dbname);

        if (!$db_select) {
            die("Błąd połączenia".mysqli_connect_error()." ERNO: ".mysqli_connect_errno());
        } else {
            echo "Połączenie z bazą danych nawiązane ",$host, "<br>";
            echo "Wybrano bazę danych: ", $dbname;

            return $connect;
        }
    }
}

function Delete($connection,$tabela,$status = "aktywny", $id_notatki = 0){

    if($status == "aktywny"){
        echo "<pre>Czy napewno chcesz usunąć notatkę</pre>";
        echo "<a href=''<button>Tak</button>";
        echo "<button>Nie</button>";



    }else {


        $sql0 = "DELETE FROM $tabela WHERE id_notatki = '$id_notatki' AND status_notatki = '$status'";

        mysqli_query($connection, $sql0);

        echo $sql0;
    }
}
function FetchOneRow($connection,$tabela,$status = "aktywny", $id_notatki = 0){
    if($tabela == ""){
        die("Błąd tabeli");


    }
    if($status == ""){
        $status = "aktywny";

    }

    $sql = "SELECT * FROM $tabela WHERE status_notatki = '$status' AND id_notatki = '$id_notatki'";
    echo $sql;

    $result = mysqli_query($connection,$sql);

    $count_rows = mysqli_num_rows($result);

    if($count_rows > 0){

        $record = mysqli_fetch_row($result);
        echo "<pre>";
        print_r($record);
        echo "</pre>";


    }else{
        echo "Brak rekordu do wyświetlenia o podanym ID", $id_notatki;

    }
}


function FetchAllRows($connection,$tabela,$status = "aktywny",$sort="DESC"){
    if($tabela == ''){
        die("Błąd tabeli");
    }
    $sql = "SELECT * FROM $tabela WHERE status_notatki = '$status'";
    $result = mysqli_query($connection,$sql);

    $count_rows = mysqli_num_rows($result);

    if($count_rows > 0){
        echo "<table class=\"table-condensed table-hover\">";
        while($record = mysqli_fetch_row($result)){
            echo "<tr>",
                "<th>ID:</th>",
                "<td",$record[0],"</td>",
            "</tr>";
            echo "<tr><th>Tytuł:</th><td>$record[1]</td></tr>";
            echo "<tr><th>Nagłówek:</th><td>$record[2]</td></tr>";
            echo "<tr><th>Treść:</th><td>$record[3]</td></tr>";
            echo "<tr><th>Data Utworzenia:</th><td>$record[4]</td></tr>";
            echo "<tr><th>Autor:</th><td>$record[5]</td></tr>";
            echo "<tr><th>Status:</th><td>$record[6]</td></tr>";
            echo "<tr><td colspan=\"4\">";
                echo "<a href='?status=" . $status . "&typ=szczegoly&id_notatki=" . $record['0'] . "'><button>Pokaż więcej</button></a>";
                echo "<a href='?status=" . $status . "&typ=usun&id_notatki=" . $record['0'] . "'><button>Usuń</button></a>";

                echo "<a href=\"#\"><button class=\"btn btn-warning\">Zrób kopię</button></a>",
                "</td></tr>";



        }
    }else{

        echo "Zapytanie nie zwróciło żadnego rekordu";
    }
}

function disconnect_db($connection){
    mysqli_close($connection);



}