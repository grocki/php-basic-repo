<pre>
    <?php
    $products = array();
    $products[11] = array('name' => 'Komputer 2013', 'price' => 322.44, 'descriptions' => 'Super komputer');
    $products[22] = array('name' => 'Komputer 2015', 'price' => 122.44, 'descriptions' => 'Super komputer');
    $products[33] = array('name' => 'Komputer 2014', 'price' => 222.44, 'descriptions' => 'Super komputer');
    $products[44] = array('name' => 'Komputer 2016', 'price' => 22.44, 'descriptions' => 'Super komputer');
    $products[55] = array('name' => 'Komputer 2017', 'price' => 422.44, 'descriptions' => 'Super komputer');
    ?>
</pre>

<!DOCTYPE html>
<html>
<head>
    <title>Portfolio</title>
    <meta charset="UTF-8">
</head>
<body>
<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    if (isset($products[$id])) {

        $taniProduktId = null;
        $taniProduktCena = 0;

        foreach ($products as $klucz => $product) {
            echo '<a href="?id=' . $klucz . '&name=' . $product['name'] . '">';
            echo $product['name'];
            echo '</a>';
            echo '<br />';


            if ($taniProduktId == null || $product['price'] < $taniProduktCena) {
                $taniProduktCena = $product['price'];
                $taniProduktId = $klucz;
            }
        }

        echo '<h1>Produkt</h1>';
        echo '<h2>' . $products[$_GET['id']]['name'] . '</h2>';
        echo '<p>' . $products[$_GET['id']]['price'] . ' zł</p>';
        echo '<small>' . $products[$_GET['id']]['descriptions'] . '</small>';

        // znajdź najtańszy produkt

        echo '<h1>Najtańszy produkt</h1>';
        echo '<h2>' . $products[$taniProduktId]['name'] . '</h2>';
        echo '<p>' . $products[$taniProduktId]['price'] . ' zł</p>';
        echo '<small>' . $products[$taniProduktId]['descriptions'] . '</small>';
    } else {
        echo "Taki produkt nie istnieje!";
    }
} else {
    echo "Brak ID!";
}
?>





<table border="1" width="100%">
    <?php if (isset($products) && !empty($products)): ?>
        <?php foreach ($products as $id => $product): ?>
            <tr bgcolor="<?php echo ($id == $taniProduktId ? "#FF0000" : ""); ?>">
                <?php foreach ($product as $kolumna => $val): ?>
                    <td><?php echo '[' . $kolumna . ']' . $val; ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>

    <?php endif; ?>
</table>


</body>
</html>