<?php
// Na podstawie parametru $_GET['id'] wybierz z Bazy Danych
// z tabeli "products" wskazany produkt
// pobrane dane tego produktu umieść w tabeli i wyświetl:
// - nazwę
// - cenę
// - opis
$result = false;

if (isset($_GET['id']) && !empty($_GET['id']) && is_numeric($_GET['id'])) {
    include 'database.php';
    $result = mysqli_query($link, "SELECT * FROM products WHERE id = " . intval($_GET['id']));
}
?>

<?php if ($result): ?>
    <?php $row = mysqli_fetch_assoc($result); ?>
    <?php if (!empty($row)): ?>
        <table width="100%" border="1">
            <tr>
                <td>Nazwa:</td>
                <td><?php echo $row['name']; ?></td>
            </tr>
            <tr>
                <td>Cena:</td>
                <td><?php echo $row['price']; ?></td>
            </tr>
            <tr>
                <td>Opis:</td>
                <td><?php echo $row['descriptions']; ?></td>
            </tr>
        </table>
        <?php mysqli_close($connect); ?>
    <?php else: ?>
        <h2>Taki produkt nie istnieje!</h2>
    <?php endif; ?>

<?php else: ?>
    <h2>Błędne zapytanie!</h2>
<?php endif; ?>