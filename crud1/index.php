<?php

include 'database.php';
$result = mysqli_query($link, "SELECT * FROM products WHERE is_active = 1");
?>

<?php if ($result): ?>
    <a href="add.php">[+ Dodaj]</a>
    <table width="100%" border="1">
        <?php while ($row = mysqli_fetch_assoc($result)): ?>
            <tr>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $row['price']; ?></td>
                <td><a href="view.php?id=<?php echo $row['id']; ?>">[View]</a><br>
                    <a href="delete.php?id=<?php echo $row['id']; ?>">[DELETE]</a>
                </td>

            </tr>
        <?php endwhile; ?>
    </table>
<?php endif; ?>

<?php mysqli_close($link); ?>