<?php



include 'database.php';

$result = mysqli_query($connect, "SELECT * FROM products WHERE is_active = 1");
?>

<?php if ($result): ?>
    <table width="100%" border="1">
        <?php while ($row = mysqli_fetch_assoc($result)): ?>
            <tr>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $row['price']; ?></td>
                <td><a href="view.php?id=<?php echo $row['id']; ?>">[View]</a>
                    <a href="edit.php?id=<?php echo $row['id']; ?>">[Edit]</a></td>
            </tr>
        <?php endwhile; ?>
    </table>
<?php endif; ?>