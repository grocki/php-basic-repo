<?php

if (isset($_GET['id']) && !empty($_GET['id']) && is_numeric($_GET['id'])) {
    include 'database.php';

    // NAPISZ PRAWIDŁOWE ZAPYTANIE SQL DO USUWANIE REKORDU Z BAZY DANYCH
    $resultDelete = mysqli_query($link, "DELETE from products WHERE id = " . intval($_GET['id']));

    // JEŚLI OPERACJĘ WYKONANO PRAWIDŁOWO WYŚWIETL KOMUNIKAT O TYM
    // W PRZECIWNYM RAZIE WYŚWIETL KOMUNIKAT BŁĘDU

    if ($resultDelete) {
        echo "<h1>Produkt usunięto!</h1>";
    } else {
        echo "Błąd: " . mysqli_error($link);
    }

    mysqli_close($link);
}
?>