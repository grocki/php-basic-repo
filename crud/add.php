<pre>
    <?php
    include 'funkcje.php';

    $errors = array();
    if (isset($_POST) && !empty($_POST)) {

        $post = array();

        foreach ($_POST as $klucz => $vartosc) {
            $errors[$klucz] = walidator($klucz);
        }

        if (sprawdz($errors)) {
            $daneDoZapisu = array();

            $daneDoZapisu['name'] = trim(ucfirst($_POST['name']));

            $daneDoZapisu['descriptions'] = trim($_POST['descriptions']);

            $daneDoZapisu['price'] = floatval($_POST['price']);

            include 'database.php';
            $resultSave = mysqli_query($link, "INSERT INTO products(price,name,descriptions) "
                . "VALUES"
                . "('" . $daneDoZapisu['price'] . "','" . $daneDoZapisu['name'] . "','" . $daneDoZapisu['descriptions'] . "' );");

            if ($resultSave) {
                echo "<h1>Produkt zapisano!</h1>";
            } else {
                echo "Błąd: " . mysqli_error($link);
            }

            mysqli_close($link);
        } else {
            echo "<h1>Popraw formularz</h1>";
        }
    }
    ?>
</pre>

<!DOCTYPE html>
<html>
<head>
    <title>Formularz</title>
    <meta charset="UTF-8">
</head>
<body>
<form method="post">
    <table>
        <tr>
            <td>Nazwa: </td>
            <td>
                <?php echo input('name', $_POST, $errors); ?>
            </td>
        </tr>
        <tr>
            <td>Cena: </td>
            <td>
                <?php echo input('price', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td>Opis: </td>
            <td>
                <?php echo input('descriptions', $_POST, $errors); ?>
            </td>
        </tr>

        <tr>
            <td></td>
            <td><input type="submit" /></td>
        </tr>
    </table>
</form>
</body>
</html>