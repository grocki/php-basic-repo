<?php
// Podaj ręsztę z dzielenia liczby1 przez liczbę2 (%)
// Jeśli któraś z liczb nie została podana w formularzu
// i nie istnieje w $_GET pokaż komunikat błędu

$wynik = "Brak wyniku";
$error = "";

if (empty($_GET['liczba1']) && empty($_GET['liczba2'])) {
    $error = "Podaj liczbę 1 i liczbę 2";
} else if (!empty($_GET['liczba1']) && empty($_GET['liczba2'])) {
    $error = "Podaj liczbę 2";
} else if (empty($_GET['liczba1']) && !empty($_GET['liczba2'])) {
    $error = "Podaj liczbę 1";
} else {

    if (is_numeric($_GET['liczba1']) && is_numeric($_GET['liczba2'])) {
        $wynik = $_GET['liczba1'] % $_GET['liczba2'];
    } else {
        $error = "Wpisz prawidłową liczbę!";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Portfolio</title>
    <meta charset="UTF-8">
</head>
<body>
<form method="get">
    <h1><?php echo $wynik; ?></h1>
    <h2><?php echo $error; ?></h2>
    <table>
        <tr>
            <td>Liczba 1: </td>
            <td>
                <input type="text" name="liczba1" />
            </td>
        </tr>
        <tr>
            <td>Liczba 2: </td>
            <td>
                <input type="text" name="liczba2" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" /></td>
        </tr>
    </table>
</form>
</body>
</html>